﻿#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Camera.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <iostream>
#include <vector>

class TerrainApplication : public Application
{
public:

	TerrainApplication()
	{
		_cameraMover = std::make_shared<FreeCameraMover>();

		lightRadius = 5.0;
		lightPhi = 0.0f;
		lightTheta = glm::pi<float>() * 0.25f;
	}

	void makeTerrains()
	{
		std::vector<std::string> terrainFilenames = { "495SopilnyakData/plain.bmp", "495SopilnyakData/makalu.bmp",
			"495SopilnyakData/mountain.bmp", "495SopilnyakData/hood.bmp", "495SopilnyakData/australia.bmp" };
		std::vector<int> factors = { 256, 128, 200, 128, 1024 };
		for (unsigned int i = 0; i < terrainFilenames.size() + 1; ++i)
		{
			terrain.push_back((i == terrainFilenames.size()) ? makeTerrain(2.0f, 200, PERLIN_NOISE, 100)
				: makeTerrain(2.5f, 200, HEIGHTMAP, factors[i], terrainFilenames[i]));
			terrain[i]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
		}
	}

	void makeTextures()
	{
		std::vector<std::string> textureFilenames = { "495SopilnyakData/snow.jpg", "495SopilnyakData/soil.jpg",
			"495SopilnyakData/lava.jpg", "495SopilnyakData/water.jpg", "495SopilnyakData/mask.png" };
		for (int i = 0; i < textureFilenames.size(); ++i)
		{
			textures.push_back(loadTexture(textureFilenames[i]));
		}

		glGenSamplers(1, &sampler);
		glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	void makeScene() override
	{
		Application::makeScene();

		ground = makeGroundPlane(3.0f, 5.0f);
		sun = makeSphere(0.1f);
		makeTerrains();

		type = 0;

		shader = std::make_shared<ShaderProgram>("495SopilnyakData/texture.vert", 
			"495SopilnyakData/texture.frag");
		sunShader = std::make_shared<ShaderProgram>("495SopilnyakData/marker.vert", "495SopilnyakData/marker.frag");

		light.position = glm::vec3(glm::cos(lightPhi) * glm::cos(lightTheta), 
			glm::sin(lightPhi) * glm::cos(lightTheta), glm::sin(lightTheta)) * lightRadius;
		light.ambient = glm::vec3(0.2, 0.2, 0.15);
		light.diffuse = glm::vec3(0.8, 0.8, 0.75);
		light.specular = glm::vec3(1.0, 1.0, 0.95);

		makeTextures();
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("Terrain", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{

			ImGui::RadioButton("Plain heightmap", &type, 0);
			ImGui::RadioButton("Makalu The Mountain heightmap", &type, 1);
			ImGui::RadioButton("Another mountain heightmap", &type, 2);
			ImGui::RadioButton("Mountain Hood heightmap", &type, 3);
			ImGui::RadioButton("Australia heightmap", &type, 4);
			ImGui::RadioButton("Perlin Noise", &type, 5);
		}
		if (ImGui::CollapsingHeader("Light"))
		{
			ImGui::SliderFloat("radius", &lightRadius, 0.1f, 10.0f);
			ImGui::SliderFloat("phi", &lightPhi, 0.0f, 2.0f * glm::pi<float>());
			ImGui::SliderFloat("theta", &lightTheta, 0.0f, glm::pi<float>());
		}
		ImGui::End();
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->use();

		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		light.position = glm::vec3(glm::cos(lightPhi) * glm::cos(lightTheta), 
			glm::sin(lightPhi) * glm::cos(lightTheta), glm::sin(lightTheta)) * lightRadius;
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(light.position, 1.0));

		shader->setVec3Uniform("light.pos", lightPosCamSpace);
		shader->setVec3Uniform("light.La", light.ambient);
		shader->setVec3Uniform("light.Ld", light.diffuse);
		shader->setVec3Uniform("light.Ls", light.specular);

		glBindSampler(0, sampler);

		std::vector<std::string> uniformNames = { "snowTex", "soilTex", "lavaTex", "waterTex", "mapTex" };
		
		for (int i = 0; i < uniformNames.size(); ++i)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			textures[i]->bind();
			shader->setIntUniform(uniformNames[i], i);
		}

		shader->setMat4Uniform("modelMatrix", terrain[type]->modelMatrix());
		shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(
			glm::mat3(_camera.viewMatrix * terrain[type]->modelMatrix()))));
		
		terrain[type]->draw();

		if (type == 5)  // Perlin Noise
		{
			ground->draw();
		}

		sunShader->use();
		sunShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), light.position));
		sunShader->setVec4Uniform("color", glm::vec4(light.diffuse, 1.0f));
		sun->draw();

		glBindSampler(0, 0);
		glUseProgram(0);
	}

private:
	MeshPtr ground;
	MeshPtr sun;
	std::vector<MeshPtr> terrain;
	ShaderProgramPtr shader;
	ShaderProgramPtr sunShader;
	int type;
	
	// Light source parameters
	LightInfo light;
	float lightRadius;
	float lightPhi;
	float lightTheta;

	// Textures
	std::vector<TexturePtr> textures;
	GLuint sampler;
};

int main()
{
	TerrainApplication app;
	app.start();

	return 0;
}
