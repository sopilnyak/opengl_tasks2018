#version 330

uniform sampler2D snowTex;
uniform sampler2D soilTex;
uniform sampler2D lavaTex;
uniform sampler2D waterTex;
uniform sampler2D mapTex;

struct LightInfo
{
	vec3 pos;
	vec3 La;
	vec3 Ld;
	vec3 Ls;
};
uniform LightInfo light;

in vec3 normalCamSpace;
in vec4 posCamSpace;
in vec2 texCoord;
in vec2 globalCoord;

out vec4 fragColor;

const vec3 Ks = vec3(1.0, 1.0, 1.0);
const float shininess = 128.0;

void main()
{
	vec3 snowTexColor = texture(snowTex, texCoord).rgb;
	vec3 lavaTexColor = texture(lavaTex, texCoord).rgb;
	vec3 soilTexColor = texture(soilTex, texCoord).rgb;
	vec3 waterTexColor = texture(waterTex, texCoord).rgb;
	vec4 mapTexColor = texture(mapTex, globalCoord).rgba;

	vec3 normal = normalize(normalCamSpace);
	vec3 viewDirection = normalize(-posCamSpace.xyz);
	
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);	

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);

	vec3 color = (lavaTexColor * mapTexColor.r + soilTexColor * mapTexColor.g + waterTexColor * mapTexColor.b + snowTexColor * (1.f - mapTexColor.a)) * 
		(light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);

		float blinnTerm = max(dot(normal, halfVector), 0.0);			
		blinnTerm = pow(blinnTerm, shininess);
		color += light.Ls * Ks * blinnTerm * (.8f * mapTexColor.b + .2f * (1.f - mapTexColor.a));
	}

	fragColor = vec4(color, 1.0);
}
