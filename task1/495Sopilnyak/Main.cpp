﻿#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Camera.hpp>

#include <iostream>
#include <vector>

class TerrainApplication : public Application
{
public:

	TerrainApplication()
	{
		_cameraMover = std::make_shared<FreeCameraMover>();
	}

	void makeScene() override
	{
		Application::makeScene();

		ground = makeGroundPlane(3.0f, 30.0f);

		std::vector<std::string> filenames = { "495SopilnyakData/plain.bmp", "495SopilnyakData/makalu.bmp",
			"495SopilnyakData/mountain.bmp", "495SopilnyakData/hood.bmp", "495SopilnyakData/australia.bmp" };
		std::vector<int> factors = { 256, 128, 200, 128, 1024 };
		for (unsigned int i = 0; i < filenames.size() + 1; ++i)
		{
			terrain.push_back((i == filenames.size()) ? makeTerrain(2.0f, 200, PERLIN_NOISE, 100)
				: makeTerrain(2.5f, 400, HEIGHTMAP, factors[i], filenames[i]));
			terrain[i]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
		}

		type = 0;

		shader = std::make_shared<ShaderProgram>("495SopilnyakData/shaderNormal.vert", "495SopilnyakData/shader.frag");
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("Terrain", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::RadioButton("Plain heightmap", &type, 0);
			ImGui::RadioButton("Makalu The Mountain heightmap", &type, 1);
			ImGui::RadioButton("Another mountain heightmap", &type, 2);
			ImGui::RadioButton("Mountain Hood heightmap", &type, 3);
			ImGui::RadioButton("Australia heightmap", &type, 4);
			ImGui::RadioButton("Perlin Noise", &type, 5);
		}
		ImGui::End();
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->use();

		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		if (type == 5)  // Perlin Noise
		{
			ground->draw();
		}
		shader->setMat4Uniform("modelMatrix", terrain[type]->modelMatrix());
		terrain[type]->draw();
	}

private:
	MeshPtr ground;
	std::vector<MeshPtr> terrain;
	ShaderProgramPtr shader;
	int type;
};

int main()
{
	TerrainApplication app;
	app.start();

	return 0;
}
