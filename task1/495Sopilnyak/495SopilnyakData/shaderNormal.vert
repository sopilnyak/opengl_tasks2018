#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    color.r = 0.2 + vertexNormal.x * 0.3 + vertexPosition.z * 0.2;
    color.g = 0.3 + vertexNormal.y * 0.3 + vertexPosition.z * 0.2;
    color.b = 0.3 + vertexNormal.z * 0.3 + vertexPosition.z * 0.2;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
