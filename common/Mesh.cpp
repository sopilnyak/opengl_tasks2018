#include <Mesh.hpp>
#include <PerlinNoise.h>
#include <HeightMap.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

MeshPtr makeGroundPlane(float size, float numTiles)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(-size, size, 0.0));
    vertices.push_back(glm::vec3(size, -size, 0.0));
    vertices.push_back(glm::vec3(size, size, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, numTiles));

    //front 2
    vertices.push_back(glm::vec3(-size, size, 0.0));
    vertices.push_back(glm::vec3(-size, -size, 0.0));
    vertices.push_back(glm::vec3(size, -size, 0.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(-numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

// Parameters: octaves for Perlin noise, factor and filename for heightmap
MeshPtr makeTerrain(float size, unsigned int frequency, HEIGHT_TYPE type, int parameter, std::string filename)
{
	std::function<float(float, float, int)> heightCalc;

	if (type == HEIGHTMAP)
	{
		HeightMap heightMap(filename);
		heightCalc = [=](float x, float y, int parameter)
		{
			return heightMap.getHeight(x, y, parameter);
		};
	}
	else
	{
		PerlinNoise perlinNoise;
		heightCalc = [=](float x, float y, int parameter)
		{
			return perlinNoise.octaveNoise(x, y, parameter);
		};
	}

	std::vector<glm::vec3> vertices;
	std::vector<std::vector<std::vector<std::pair<int, glm::vec3>>>> norms(frequency + 2,
		std::vector<std::vector<std::pair<int, glm::vec3>>>(frequency + 2,
			std::vector<std::pair<int, glm::vec3>>()));
	std::vector<glm::vec2> texcoords;
	std::vector<glm::vec2> globalcoords;

	float step = 2.0f / frequency;
	float textureQuality = 3.f;

	int it = 0;
	int i_count = 0;
	for (float i = -1.; i < 1.; i += step)
	{
		int j_count = 0;
		for (float j = -1.; j < 1.; j += step)
		{
			// First triangle
			glm::vec3 vertice1 = glm::vec3(i * size, j * size, heightCalc(i, j, parameter));
			glm::vec3 vertice2 = glm::vec3((i + step) * size, j * size, heightCalc(i + step, j, parameter));
			glm::vec3 vertice3 = glm::vec3(i * size, (j + step) * size, heightCalc(i, j + step, parameter));

			if (vertice1.z >= 0 && vertice2.z >=0 && vertice3.z >=0)
			{
				vertices.push_back(vertice1);
				vertices.push_back(vertice2);
				vertices.push_back(vertice3);

				glm::vec3 norm = glm::normalize(glm::cross(vertice3 - vertice1, vertice2 - vertice1));

				norms[i_count][j_count].emplace_back(it++, -norm);
				norms[i_count + 1][j_count].emplace_back(it++, -norm);
				norms[i_count][j_count + 1].emplace_back(it++, -norm);

				texcoords.push_back(glm::vec2((i + 1.f) * textureQuality, (j + 1.f) * textureQuality));
				texcoords.push_back(glm::vec2((i + 1.f) * textureQuality + step * textureQuality, (j + 1.f) * textureQuality));
				texcoords.push_back(glm::vec2((i + 1.f) * textureQuality, (j + 1.f) * textureQuality + step * textureQuality));
				
				float globalX = (i + 1.f) / 2.f;
				float globalY = (j + 1.f) / 2.f;
				globalcoords.push_back(glm::vec2(globalX, globalY));
				globalcoords.push_back(glm::vec2(globalX + step / 2.f, globalY));
				globalcoords.push_back(glm::vec2(globalX, globalY + step / 2.f));
			}

			// Second triangle
			vertice1 = glm::vec3(i * size, (j + step) * size, heightCalc(i, j + step, parameter));
			vertice2 = glm::vec3((i + step) * size, (j + step) * size, heightCalc(i + step, j + step, parameter));
			vertice3 = glm::vec3((i + step) * size, j * size, heightCalc(i + step, j, parameter));

			if (vertice1.z >= 0 && vertice2.z >= 0 && vertice3.z >= 0)
			{
				vertices.push_back(vertice1);
				vertices.push_back(vertice2);
				vertices.push_back(vertice3);

				glm::vec3  norm = glm::normalize(glm::cross(vertice3 - vertice1, vertice2 - vertice1));
				norms[i_count][j_count + 1].emplace_back(it++, norm);
				norms[i_count + 1][j_count + 1].emplace_back(it++, norm);
				norms[i_count + 1][j_count].emplace_back(it++, norm);

				texcoords.push_back(glm::vec2((i + 1.f) * textureQuality, (j + 1.f) * textureQuality + step * textureQuality));
				texcoords.push_back(glm::vec2((i + 1.f) * textureQuality + step * textureQuality, 
					(j + 1.f) * textureQuality + step * textureQuality));
				texcoords.push_back(glm::vec2((i + 1.f) * textureQuality + step * textureQuality, (j + 1.f) * textureQuality));

				float globalX = (i + 1.f) / 2.f;
				float globalY = (j + 1.f) / 2.f;
				globalcoords.push_back(glm::vec2(globalX, globalY));
				globalcoords.push_back(glm::vec2(globalX + step / 2.f, globalY));
				globalcoords.push_back(glm::vec2(globalX, globalY + step / 2.f));
			}
			++j_count;
		}
		++i_count;
	}

	// Interpolated norms
	std::vector<glm::vec3> interpolatedNorms;
	interpolatedNorms = std::vector<glm::vec3>(it);
	for (unsigned int i = 0; i < frequency; ++i)
	{
		for (unsigned int j = 0; j < frequency; ++j)
		{
			glm::vec3 norm;
			for (auto& element : norms[i][j])
			{
				norm += element.second;
			}
			norm /= norms[i][j].size();
			for (auto& element : norms[i][j])
			{
				interpolatedNorms[element.first] = norm;
			}
		}
	}

	DataBufferPtr bufVertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	bufVertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr bufNorms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	bufNorms->setData(interpolatedNorms.size() * sizeof(float) * 3, interpolatedNorms.data());

	DataBufferPtr bufTexture = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	bufTexture->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	DataBufferPtr bufGlobal = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	bufGlobal->setData(globalcoords.size() * sizeof(float) * 2, globalcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, bufVertices);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, bufNorms);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, bufTexture);
	mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, bufGlobal);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(static_cast<GLuint>(vertices.size()));

	std::cout << "Terrain is loaded with " << vertices.size() << " vertices\n";

	return mesh;
}

MeshPtr makeSphere(float radius, unsigned int N)
{
	unsigned int M = N / 2;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

	for (unsigned int i = 0; i < M; i++)
	{
		float theta = (float)glm::pi<float>() * i / M;
		float theta1 = (float)glm::pi<float>() * (i + 1) / M;

		for (unsigned int j = 0; j < N; j++)
		{
			float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
			float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

			vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

			normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

			vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
			vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

			normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
			normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
		}
	}

	//----------------------------------------

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

	return mesh;
}
