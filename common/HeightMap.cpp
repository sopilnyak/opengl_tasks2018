#include <Mesh.hpp>
#include <HeightMap.h>

#include <string>

HeightMap::HeightMap(std::string filename)
{
	FILE* f = fopen(filename.c_str(), "rb");
	unsigned char info[54];

	fread(info, sizeof(unsigned char), 54, f);  // Header

	int width = *(int*)&info[18];
	int height = *(int*)&info[22];

	int row_padded = (width * 3 + 3) & (~3);
	unsigned char* data = new unsigned char[row_padded];

	for (int i = 0; i < height; ++i)
	{
		fread(data, sizeof(unsigned char), row_padded, f);
		std::vector<float> row;
		for (int j = 0; j < width * 3; j += 3)
		{
			row.push_back((data[j] + data[j + 1] + data[j + 2]) / 3);
		}
		imageBuffer.push_back(row);
	}

	delete[] data;

	fclose(f);
}

float HeightMap::getHeight(float x, float y, int factor) const
{
	int xCoord = (int)((1.0 + x) / 2.0 * imageBuffer[0].size());
	int yCoord = (int)((1.0 + y) / 2.0 * imageBuffer.size());
	return imageBuffer[yCoord % imageBuffer.size()][xCoord % imageBuffer[0].size()] / factor;
}
