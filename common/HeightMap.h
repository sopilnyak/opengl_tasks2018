#include <iostream>
#include <vector>

class HeightMap
{
public:
	explicit HeightMap(std::string filename);
	float getHeight(float x, float y, int factor = 128) const;

private:
	std::vector<std::vector<float>> imageBuffer;
	unsigned int width;
	unsigned int height;
};
