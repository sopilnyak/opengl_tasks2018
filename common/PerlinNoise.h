#include <numeric>
#include <algorithm>
#include <random>

class PerlinNoise
{
public:

	explicit PerlinNoise(std::uint32_t seed = std::default_random_engine::default_seed)
	{
		reseed(seed);
	}

	void reseed(std::uint32_t seed)
	{
		for (size_t i = 0; i < 256; ++i)
		{
			p[i] = i;
		}

		std::shuffle(std::begin(p), std::begin(p) + 256, std::default_random_engine(seed));

		for (size_t i = 0; i < 256; ++i)
		{
			p[256 + i] = p[i];
		}
	}

	float noise(float x, float y) const
	{
		return noise(x, y, 0.0);
	}

	float noise(float x, float y, float z) const
	{
		const std::int32_t X = static_cast<std::int32_t>(std::floor(x)) & 255;
		const std::int32_t Y = static_cast<std::int32_t>(std::floor(y)) & 255;
		const std::int32_t Z = static_cast<std::int32_t>(std::floor(z)) & 255;

		x -= std::floor(x);
		y -= std::floor(y);
		z -= std::floor(z);

		const float u = fade(x);
		const float v = fade(y);
		const float w = fade(z);

		const std::int32_t A = p[X] + Y, AA = p[A] + Z, AB = p[A + 1] + Z;
		const std::int32_t B = p[X + 1] + Y, BA = p[B] + Z, BB = p[B + 1] + Z;

		return lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z),
			grad(p[BA], x - 1, y, z)),
			lerp(u, grad(p[AB], x, y - 1, z),
				grad(p[BB], x - 1, y - 1, z))),
			lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1),
				grad(p[BA + 1], x - 1, y, z - 1)),
				lerp(u, grad(p[AB + 1], x, y - 1, z - 1),
					grad(p[BB + 1], x - 1, y - 1, z - 1))));
	}

	float octaveNoise(float x, float y, std::int32_t octaves) const
	{
		float result = 0.0;
		float amplitude = 1.0;

		for (std::int32_t i = 0; i < octaves; ++i)
		{
			result += noise(x, y) * amplitude;
			x *= 2.0;
			y *= 2.0;
			amplitude *= 0.5;
		}

		return result;
	}

	float noise0_1(float x, float y) const
	{
		return noise(x, y) * 0.5 + 0.5;
	}

	float noise0_1(float x, float y, float z) const
	{
		return noise(x, y, z) * 0.5 + 0.5;
	}

	float octaveNoise0_1(float x, float y, std::int32_t octaves) const
	{
		return octaveNoise(x, y, octaves) * 0.5 + 0.5;
	}

private:

	std::int32_t p[512];

	static float fade(float t) noexcept
	{
		return t * t * t * (t * (t * 6 - 15) + 10);
	}

	static float lerp(float t, float a, float b) noexcept
	{
		return a + t * (b - a);
	}

	static float grad(std::int32_t hash, float x, float y, float z) noexcept
	{
		const std::int32_t h = hash & 15;
		const float u = h < 8 ? x : y;
		const float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
		return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
	}

};
